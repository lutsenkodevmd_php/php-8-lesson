<?php


    /**
     * Class ShopProduct базовый
     */
    class ShopProduct{

        public $title;
        public $producerFirstName;
        public $producerMainName;
        public $price;
        public $numPage;
        public $playLength;


        public function __construct(
            string $title,
            string $producerFirstName="",
            string $producerMainName="",
            float $price=0
            )
        {
            $this->title=$title;
            $this->producerFirstName=$producerFirstName;
            $this->producerMainName=$producerMainName;
            $this->price=$price;
        }
        // Методы класса
        public function getProducer(){
            return $this->producerFirstName." ".$this->producerMainName;
        }
        public function getSummaryLine():string
        {
            $base = "{$this->title} ( {$this->producerMainName}, ";
            $base.= "{$this->producerFirstName} ";
            return $base;
        }
    }

    class CDProduct extends ShopProduct
    {
        protected $playLength=0;

        public function __construct(
            string $title,
            string $producerFirstName = "",
            string $producerMainName = "",
            float $price = 0,
            int $playLength=0)
        {
            parent::__construct($title, $producerFirstName, $producerMainName, $price);
            $this->playLength=$playLength;
        }

        public function getPlayLength():int{
            return $this->playLength;
        }
        public function getSummaryLine(): string
        {
            $base = parent::getSummaryLine();
            $base.= "Время : {$this->playLength}  )";
            return $base;
        }
    }

    class BookProduct extends ShopProduct{
        protected $numPage;

        public function __construct(
            string $title,
            string $producerFirstName = "",
            string $producerMainName = "",
            float $price = 0,
            int $numPage=0)
        {
            parent::__construct($title, $producerFirstName, $producerMainName, $price);
            $this->numPage=$numPage;
        }

        public function getNumberOfPage():int{
            return $this->numPage;
        }
        public function getSummaryLine(): string
        {

            $base = parent::getSummaryLine();
            $base.= " {$this->numPage}  стр. )";
            return $base;
        }
    }


    // Создадим первые классы
    $product1 = new BookProduct(
        "Собачье сердце",
        "Михаил",
        "Булгаков",
        5.99,
        200);

    print $product1->getSummaryLine();

    echo  "<hr>";

    $product2 = new CDProduct("Классическое музыка",
        "Antonio",
        "Vilde",
        300,120);

    print $product2->getSummaryLine();