<?php

    // Начала класса
    class ShopProduct  {
        //  свойства класса
        public $title;
        public $producerMainName;
        public $producerFirstNmae;
        public $price;

        public function  __construct($title,$firstName,$mainName,$price)
        {
            $this->title=$title;
            $this->producerFirstNmae=$firstName;
            $this->producerMainName=$mainName;
            $this->price=$price;
        }


        // Методы класса
        public function getProducer(){
            return $this->producerFirstNmae." ".$this->producerMainName;
        }
    }


    /**
     * Class ShopProduct8 Класс согласно изменений верси php 8
     */
    class ShopProduct8{
        public function __construct(
            public $title,
            public $producerFirstNmae="",
            public $producerMainName="",
            public $price=0,
            )
        {
        }
        // Методы класса
        public function getProducer(){
            return $this->producerFirstNmae." ".$this->producerMainName;
        }
    }


    // Создадим первые классы
    $product1 = new ShopProduct("Собачье сердце","Михаил","Булгаков",5.99);
    // Создадим классса

    $product2_1 = new ShopProduct8("Собачье сердце");
    $product2 = new ShopProduct8(
                            title:"Собачье сердце",
                            producerMainName:"Test_name");



    print "Автор : {$product1->getProducer()}";
    print "Автор : {$product2->getProducer()}";


